Tallgame is a personal Minecraft-like game which I am developing
to learn from and experiment with new ideas.
It is coded in C++ using OpenGL through
the GLFW and GLEW libraries, as well as GLM.

Project status: On hold.
Very early development. Several core features,
such as world save/load and multiplayer, are not implemented.
The game is currently playable but needs a lot of work.

Everything in Tallgame was made by me,
except for the current placeholder block textures.