#include "Chunk.h"



Chunk::Chunk(int x, int y, int z)
{
	pos = glm::vec3(x, y, z);

	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);

	for (int x = 0; x < 16; x++) {
		for (int y = 0; y < 16; y++) {
			for (int z = 0; z < 16; z++) {
				blocks[x][y][z] = 0;
			}
		}
	}

	genVertexArray();
}


Chunk::~Chunk()
{
	//delete[] vertexData;
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
}



void Chunk::genVertexArray() {
	std::vector<GLfloat> vertexVector;
	//vertexCount = blockCount * 6 * 6;
	vertexCount = 0;

	for (int x = 0; x < 16; x++) {
		for (int y = 0; y < 16; y++) {
			for (int z = 0; z < 16; z++) {

				if (blocks[x][y][z] != 0) {

					//for (int i = 0; i < 288; i++) {
					for (int i = 0; i < 180; i++) {
						//std::cout << i << std::endl;
						bool skipLoop = false;
						if (i % 30 == 0) { //beginning of face
							if ((i / 30 == 0 && x < 15 && blocks[x + 1][y][z] != 0) // +x face
								|| (i / 30 == 1 && x > 0 && blocks[x - 1][y][z] != 0) // -x face
								|| (i / 30 == 2 && y < 15 && blocks[x][y + 1][z] != 0) // +y face
								|| (i / 30 == 3 && y > 0 && blocks[x][y - 1][z] != 0) // -y face
								|| (i / 30 == 4 && z < 15 && blocks[x][y][z + 1] != 0) // +z face
								|| (i / 30 == 5 && z > 0 && blocks[x][y][z - 1] != 0)) // -z face
							{
								i += 29;
								skipLoop = true;
							}
						}

						if (!skipLoop) {
							GLfloat val = blockModel[i];
							if (i % 5 == 0) {
								val += x;
							}
							else if (i % 5 == 1) {
								val += y;
							}
							else if (i % 5 == 2) {
								val += z;
							}
							else if (i % 5 == 3) { //previously i % 8 == 6
								float texX = ((blocks[x][y][z] % 10) / 10.0f) - 0.1f;
								val = (val / 10) + texX;
							}
							else if (i % 5 == 4) {
								float texY = (blocks[x][y][z] / 10) - 0.1f;
								val = (val / 10) + texY;
							}
							vertexVector.push_back(val);
							vertexCount++;
						}

					}

				}
				
			}
		}
	}

	GLfloat *vertexData = vertexVector.data();
	size_t data_len = vertexVector.size() * sizeof(GLfloat);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, data_len, vertexData, GL_STATIC_DRAW);

	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid *)0);
	glEnableVertexAttribArray(0);
	//Color attribute
	//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	//glEnableVertexAttribArray(1);
	//Texture attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	
}



glm::vec3 Chunk::getPos() { return pos; }
GLuint Chunk::getVAO() { return VAO; }
int Chunk::getVertexCount() { return vertexCount; }


void Chunk::setBlocks(int bArray[][16][16]) {
	for (int x = 0; x < 16; x++) {
		for (int y = 0; y < 16; y++) {
			for (int z = 0; z < 16; z++) {
				blocks[x][y][z] = bArray[x][y][z];
			}
		}
	}
	genVertexArray();
}


void Chunk::setBlock(int x, int y, int z, int id) {
	blocks[x][y][z] = id;
	genVertexArray();
}

int Chunk::getBlock(int x, int y, int z) {
	return blocks[x][y][z];
}


void Chunk::breakBlock(glm::vec3 pos) {

	if (blocks[int(pos.x)][int(pos.y)][int(pos.z)] != 0) {
		setBlock(pos.x, pos.y, pos.z, 0);
	}

}