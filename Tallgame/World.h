#pragma once

#ifndef WORLD_H
#define WORLD_H

#include<glm/glm.hpp>
#include<vector>
#include<string>
#include<tuple>
#include<fstream>

#include<dirent.h>
#include <windows.h>

#include"Chunk.h"
#include"Entity.h"
#include"ChunkProvider.h"
#include"AABB.h"

class World
{
public:
	World();
	~World();

	void tick(float deltaTime);

	std::tuple<glm::vec3, glm::vec3> findBlockOnRay(glm::vec3 startPos, glm::vec3 camFront, float maxDist);
	int getBlock(int x, int y, int z);
	void breakBlock(glm::vec3 pos);
	void addBlock(glm::vec3 pos, int type);

	void loadWorld();
	Chunk* loadChunk(glm::vec3 pos);
	void save();
	void saveChunk(Chunk* c);

	Chunk* getChunk(int x, int y, int z);
	Chunk* genChunk(int x, int y, int z);
	void removeChunk(int x, int y, int z);
	std::vector<Chunk*>* getChunks();

	void addEntity(Entity* newEntity);
	std::vector<Entity*>* getEntities();

	void addPlayerEntity(Entity* newEntity);

	void setRenderDistance(int rd);
private:
	typedef std::tuple<glm::vec3, glm::vec3> raycastReturn;

	int seed;
	int renderDistance;
	std::string WORLD_PATH;

	const float SAVE_VERSION = 0.1;

	ChunkProvider chprovider;
	std::vector<glm::vec3> loadList;
	std::vector<glm::vec3> unloadList;

	std::vector<Chunk*> chunks;
	std::vector<Entity*> entities;
	std::vector<Entity*> playerEntities;


	void genLoadList(glm::vec3 startChunk);
	void genUnloadList(glm::vec3 startChunk);
	bool checkCollide(glm::vec3 start, glm::vec3 end);
};

#endif