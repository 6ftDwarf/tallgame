#pragma once

#ifndef CHUNKPROVIDER_H
#define CHUNKPROVIDER_H

#include"Chunk.h"
#include"PerlinNoise.h"
#include"WorleyNoise.h"
#include"MathHelper.h"

#include<GLFW/glfw3.h>


class ChunkProvider
{
public:
	ChunkProvider();
	~ChunkProvider();

	Chunk* buildChunk(Chunk* chunk, int seed);
private:
	MathHelper mhelp;
};

#endif