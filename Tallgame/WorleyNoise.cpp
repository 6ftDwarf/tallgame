#include "WorleyNoise.h"



WorleyNoise::WorleyNoise()
{
}


WorleyNoise::~WorleyNoise()
{
}


std::vector<std::vector<float>> WorleyNoise::makeHeightmap(int width, int height, glm::vec2 startPos, int seed, float frequency, float heightScale, int minPoints, int maxPoints) {
	
	std::vector<std::vector<float>> ret(width);

	for (int x = 0; x < width; x++) {
		ret[x] = std::vector<float>(height);
		for (int y = 0; y < height; y++) {
			ret[x][y] = 0;
		}
	}

	startPos.x /= frequency;
	startPos.y /= frequency;

	//These include the squares around the ones the heightmap uses because they might have points closer to
	//a pixel than the points in the heightmap squares
	int leftEdge = floor(startPos.x) - 1;
	int rightEdge = ceil(startPos.x + width / frequency) + 1;
	int lowEdge = floor(startPos.y) - 1;
	int topEdge = ceil(startPos.y + height / frequency) + 1;

	std::vector<glm::vec2> points;

	for (int x = leftEdge; x < rightEdge; x++) {
		for (int y = lowEdge; y < topEdge; y++) {

			srand(((seed * 71) + ((x) * 73) + ((y) * 79)) % 83);
			int pointsThisSquare = (rand() % (maxPoints - minPoints)) + minPoints;
			for (int i = 0; i < pointsThisSquare; i++) {
				points.push_back(glm::vec2(x + (rand() % 1000) / 1000.0f, y + (rand() % 1000) / 1000.0f));
			}

		}
	}

	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {

			glm::vec2 current = glm::vec2(startPos.x + x / frequency, startPos.y + y / frequency);
			glm::vec2 nearest;
			float nearestDist = 100000000;
			for (int i = 0; i < points.size(); i++) {
				if (glm::distance(points.at(i), current) < nearestDist) {
					nearest = points.at(i);
					nearestDist = glm::distance(points.at(i), current);
				}
			}

			ret[x][y] = nearestDist * heightScale;

		}
	}

	return ret;

}