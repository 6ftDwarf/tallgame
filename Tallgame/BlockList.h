#pragma once

#ifndef BLOCKLIST_H
#define BLOCKLIST_H

#include<vector>
#include"Block.h"

#include"BlockUglyStone.h"
#include"BlockUglyGrass.h"

extern std::vector<Block*> list; //GLOBAL VARIABLE

class BlockList
{
public:
	BlockList();
	~BlockList();

	Block* getBlock(int id);
	void addBlock(Block* b);

	static void initBlocks();
	static void cleanList();

private:
	//std::vector<Block> list;
};

#endif