#pragma once

#ifndef WORLEYNOISE_H
#define WORLEYNOISE_H

#include<array>
#include<vector>
#include<iostream>

#include<glm/glm.hpp>

#include"MathHelper.h"


class WorleyNoise
{
public:
	WorleyNoise();
	~WorleyNoise();

	static std::vector<std::vector<float>> makeHeightmap(int width, int height, glm::vec2 startPos, int seed, float frequency, float heightScale, int minPoints, int maxPoints);
};

#endif