#pragma once

#ifndef TARGETBOX_H
#define TARGETBOX_H

#define GLEW_STATIC
#include <GL/glew.h>

#include<glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>

#include "ShaderMaker.h"


class TargetBox
{
public:
	TargetBox();
	~TargetBox();

	void useShader();

	GLuint getVAO();

	glm::vec3 getPos();
	void setPos(glm::vec3 p);

	void setRenderBox(bool b);
	bool shouldRenderBox();

	void setModelLoc();
	void setViewLoc(glm::mat4 view);
	void setProjectionLoc(glm::mat4 projection);
private:
	glm::vec3 pos;
	bool renderBox;

	GLuint VAO, VBO;
	GLuint modelLoc, viewLoc, projectionLoc;
	const GLchar *vertexShaderPath = ".\\Resources\\VShaderLine.txt";
	const GLchar *fragmentShaderPath = ".\\Resources\\FShaderLine.txt";

	ShaderMaker shader = ShaderMaker(vertexShaderPath, fragmentShaderPath);
};

#endif