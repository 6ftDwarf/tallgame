#pragma once

#ifndef BLOCK_H
#define BLOCK_H

#include<glm\glm.hpp>

class Block
{
public:
	Block();
	~Block();

	int getId();
	glm::vec2 getTexCoords();
protected:
	int id;
	glm::vec2 texCoords;
};

#endif