#include "ChunkProvider.h"



ChunkProvider::ChunkProvider()
{
}


ChunkProvider::~ChunkProvider()
{
}



Chunk* ChunkProvider::buildChunk(Chunk* chunk, int seed) {
	//srand(seed);
	
	int cornerValues[2][2];
	//cornerValues[0][0] = rand() % 50 + 1;
	cornerValues[0][0] = rand() % 50 + 1;
	cornerValues[1][0] = 20;
	cornerValues[0][1] = 0;
	cornerValues[1][1] = 20;

	glm::vec3 chunkPos = chunk->getPos();

	//std::vector<std::vector<float>> heightMap = PerlinNoise::makeHeightmap(16, 16, chunkPos.x * 16, chunkPos.z * 16, seed, 32.0f, 32);
	std::vector<std::vector<float>> heightMap = PerlinNoise::makeHeightmapWithOctaves(16, 16, glm::vec2(chunkPos.x * 16, chunkPos.z * 16), seed, 6, 32.0f * 6, 0.5f, 32.0f * 6, 0.5f);
	//std::vector<std::vector<float>> heightMap = WorleyNoise::makeHeightmap(16, 16, glm::vec2(chunkPos.x * 16, chunkPos.z * 16), seed, 64, 32, 1, 4);


	int blocks[16][16][16];
	for (GLuint x = 0; x < 16; x++) {
		for (GLuint z = 0; z < 16; z++) {
			//int columnHeight = rand() % 3;
			//int lerpX1 = mhelp.lerp(glm::vec2(0, cornerValues[0][0]), glm::vec2(15, cornerValues[1][0]), x);
			//int lerpX2 = mhelp.lerp(glm::vec2(0, cornerValues[0][1]), glm::vec2(15, cornerValues[1][1]), x);
			//int lerpZ = mhelp.lerp(glm::vec2(0, lerpX1), glm::vec2(15, lerpX2), z);
			//float lerpZ = PerlinNoise::getValue((x + chunkPos.x * 16) / 16.0f + 0.0001f, (z + chunkPos.z * 16) / 16.0f + 0.0001f, seed);
			float lerpZ = heightMap[x][z];
			//std::cout << lerpZ << std::endl;
			for (GLuint y = 0; y < 16; y++) {

				if (chunk->getPos().y * 16 + y <= lerpZ - 1) {
					/*
					int sideCoords = 0;
					if (x == 0 || x == 15) {
						sideCoords++;
					}
					if (y == 0 || y == 15) {
						sideCoords++;
					}
					if (z == 0 || z == 15) {
						sideCoords++;
					}
					if (sideCoords == 2) {
						blocks[x][y][z] = 1;
					}
					else if (sideCoords == 3) {
						blocks[x][y][z] = 2;
					}
					else {
						blocks[x][y][z] = 0;
					}
					*/
					blocks[x][y][z] = 1;
				}
				else if (chunk->getPos().y * 16 + y <= lerpZ) {
					blocks[x][y][z] = 2;
				}
				else {
					blocks[x][y][z] = 0;
				}
				
			}
		}
	}

	float vStartTime = glfwGetTime();

	chunk->setBlocks(blocks);
	
	//std::cout << "vertex gen time " << (glfwGetTime() - vStartTime) * 1000 << " milliseconds" << std::endl;

	return chunk;
}