#version 330 core

//in vec3 vertexColor;
in vec2 TexCoord;

out vec4 color;

uniform sampler2D myTexture1;

void main( )
{
	color = texture(myTexture1, TexCoord);
}