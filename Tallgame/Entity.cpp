#include "Entity.h"



Entity::Entity()
{

	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);

	// Temporary initializer
	aabb = AABB(glm::vec3(1, 2, 1), pos);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(entityModel), entityModel, GL_STATIC_DRAW);

	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid *)0);
	glEnableVertexAttribArray(0);
	//Color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	//Texture attribute
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}


Entity::~Entity()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
}



glm::vec3 Entity::getPos() { return pos; }
void Entity::setPos(glm::vec3 newPos) { prevPos = pos; pos = newPos; aabb.setPos(newPos); }

glm::vec3 Entity::getPrevPos() { return prevPos; }

AABB* Entity::getAABB() { return &aabb; }
void Entity::setAABBsize(glm::vec3 newSize) { aabb.setSize(newSize); }

bool Entity::isOnGround() { return onGround; }
void Entity::setOnGround(bool ong) { onGround = ong; }

glm::vec3 Entity::getVel() { return velocity; }
void Entity::setVel(glm::vec3 newVel) { velocity = newVel; }

float Entity::getMass() { return mass; }
void Entity::setMass(float newMass) { mass = newMass; }

glm::vec3 Entity::getBFront() { return bodyFront; }
void Entity::setBFront(glm::vec3 newFront) { bodyFront = newFront; }

bool Entity::shouldRender() { return renderBody; }
void Entity::setRender(bool r) { renderBody = r; }

GLuint Entity::getVAO() { return VAO; }
int Entity::getVertexCount() { return vertexCount; }