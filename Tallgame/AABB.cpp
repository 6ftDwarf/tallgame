#include "AABB.h"



AABB::AABB()
{
}

AABB::AABB(glm::vec3 startsize, glm::vec3 startPos) {
	size = startsize;
	pos = startPos;
}


AABB::~AABB()
{
}


bool AABB::collideCheck(AABB* aabb2) {
	glm::vec3 size2 = aabb2->getSize();
	glm::vec3 pos2 = aabb2->getPos();

	bool ret = true;

	if (pos.x + size.x < pos2.x || pos.x > pos2.x + size2.x) { ret = false; }
	else if (pos.y + size.y < pos2.y || pos.y > pos2.y + size2.y) { ret = false; }
	else if (pos.z + size.z < pos2.z || pos.z > pos2.z + size2.z) { ret = false; }

	return ret;
}


glm::vec3 AABB::getSize() { return size; }
void AABB::setSize(glm::vec3 newsize) { size = newsize; }

glm::vec3 AABB::getPos() { return pos; }
void AABB::setPos(glm::vec3 newPos) { pos = newPos; }