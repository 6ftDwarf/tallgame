#pragma once

#ifndef TARGETRETICLE_H
#define TARGETRETICLE_H

#define GLEW_STATIC
#include <GL/glew.h>

#include<glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>

#include "ShaderMaker.h"


class TargetReticle
{
public:
	TargetReticle();
	~TargetReticle();

	void useShaderAndTexture();
	void closeTexture();

	GLuint getVAO();
private:
	GLuint VAO, VBO;
	const GLchar *vertexShaderPath = ".\\Resources\\VShaderReticle.txt";
	const GLchar *fragmentShaderPath = ".\\Resources\\FShaderReticle.txt";

	GLuint reticleTexture;

	ShaderMaker shader = ShaderMaker(vertexShaderPath, fragmentShaderPath);
};

#endif