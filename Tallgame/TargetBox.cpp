#include "TargetBox.h"



TargetBox::TargetBox()
{
	shader = ShaderMaker(vertexShaderPath, fragmentShaderPath);

	modelLoc = shader.getUniform("model");
	viewLoc = shader.getUniform("view");
	projectionLoc = shader.getUniform("projection");

	setPos(glm::vec3(0, 0, 0));

	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	GLfloat vertices[] = { //If this array is put in the header, it reacts to changes really weirdly
		0.5f, 0.5f, -0.5f,		0.4f, 0.4f, 0.4f,//
		0.5f, 0.5f, 0.5f,		0.4f, 0.4f, 0.4f,
		0.5f, -0.5f, -0.5f,		0.4f, 0.4f, 0.4f,//
		0.5f, -0.5f, 0.5f,		0.4f, 0.4f, 0.4f,
		-0.5f, 0.5f, -0.5f,		0.4f, 0.4f, 0.4f,//
		-0.5f, 0.5f, 0.5f,		0.4f, 0.4f, 0.4f,
		-0.5f, -0.5f, -0.5f,	0.4f, 0.4f, 0.4f,//
		-0.5f, -0.5f, 0.5f,		0.4f, 0.4f, 0.4f,

		0.5f, 0.5f, -0.5f,		0.4f, 0.4f, 0.4f,//
		-0.5f, 0.5f, -0.5f,		0.4f, 0.4f, 0.4f,
		0.5f, -0.5f, -0.5f,		0.4f, 0.4f, 0.4f,//
		-0.5f, -0.5f, -0.5f,	0.4f, 0.4f, 0.4f,
		0.5f, 0.5f, 0.5f,		0.4f, 0.4f, 0.4f,//
		-0.5f, 0.5f, 0.5f,		0.4f, 0.4f, 0.4f,
		0.5f, -0.5f, 0.5f,		0.4f, 0.4f, 0.4f,//
		-0.5f, -0.5f, 0.5f,		0.4f, 0.4f, 0.4f,

		0.5f, 0.5f, -0.5f,		0.4f, 0.4f, 0.4f,//
		0.5f, -0.5f, -0.5f,		0.4f, 0.4f, 0.4f,
		-0.5f, 0.5f, -0.5f,		0.4f, 0.4f, 0.4f,//
		-0.5f, -0.5f, -0.5f,	0.4f, 0.4f, 0.4f,
		0.5f, 0.5f, 0.5f,		0.4f, 0.4f, 0.4f,//
		0.5f, -0.5f, 0.5f,		0.4f, 0.4f, 0.4f,
		-0.5f, 0.5f, 0.5f,		0.4f, 0.4f, 0.4f,//
		-0.5f, -0.5f, 0.5f,		0.4f, 0.4f, 0.4f
	};

	size_t data_len = sizeof(vertices);
	glBufferData(GL_ARRAY_BUFFER, data_len, vertices, GL_STATIC_DRAW);

	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid *)0);
	glEnableVertexAttribArray(0);
	//Color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}


TargetBox::~TargetBox()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
}


void TargetBox::useShader() { shader.Use(); }

GLuint TargetBox::getVAO() { return VAO; }

glm::vec3 TargetBox::getPos() { return pos; }
void TargetBox::setPos(glm::vec3 p) { pos = p; }

bool TargetBox::shouldRenderBox() { return renderBox; }
void TargetBox::setRenderBox(bool b) { renderBox = b; }

void TargetBox::setModelLoc() {
	glm::mat4 model;
	model = glm::translate(model, glm::vec3(pos.x, pos.y, pos.z));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
}
void TargetBox::setViewLoc(glm::mat4 view){ glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view)); }
void TargetBox::setProjectionLoc(glm::mat4 projection) { glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection)); }