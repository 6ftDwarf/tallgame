#include "PerlinNoise.h"



PerlinNoise::PerlinNoise()
{
}


PerlinNoise::~PerlinNoise()
{
}


//x0 and y0 are the starting coordinate for the heightmap. x0 is x==0 on the heightmap
std::vector<std::vector<float>> PerlinNoise::makeHeightmap(int width, int height, float x0, float y0, int seed, float frequency, int heightScale) {
	std::vector<std::vector<float>> ret(width);

	//float startTime = glfwGetTime();

	x0 /= frequency;
	y0 /= frequency;

	//Maybe get rid of these loops, just have them later
	for (int x = 0; x < width; x++) {
		ret[x] = std::vector<float>(height);
		for (int y = 0; y < height; y++) {
			ret[x][y] = 0;
		}
	}

	int leftLineX = floor(x0); //the leftmost grid line
	int rightLineX = ceil(x0 + width / frequency); //the rightmost grid line
	int topLineY = ceil(y0 + height / frequency); //the uppermost grid line
	int bottomLineY = floor(y0); //the lowest grid line

	std::vector<std::vector<glm::vec2>> gradientVecs;
	for (int x = 0; x <= rightLineX - leftLineX; x++) {
		//std::cout << "before" << std::endl;
		//gradientVecs[x] = std::vector<glm::vec2>(topY - bottomY);
		gradientVecs.push_back(std::vector<glm::vec2>(topLineY - bottomLineY + 1));
		//std::cout << "after" << std::endl;
		for (int y = 0; y <= topLineY - bottomLineY; y++) {
			srand(((seed * 71) + ((x + leftLineX) * 73) + ((y + bottomLineY) * 79)) % 83);
			gradientVecs[x][y] = glm::vec2(rand() % heightScale, rand() % heightScale);
			//std::cout << "gVec at " << x + leftX << "," << y + bottomY << " is " << gradientVecs[x][y].x << "," << gradientVecs[x][y].y << std::endl;
		}
	}

	for (int x = 0; x < width; x++) {

		float worldX = x0 + x / frequency; //the current x coordinate to get the height value of
		int leftSide = floor(worldX) - leftLineX; //the left x coordinate for gradientVecs
		int rightSide = ceil(worldX) - leftLineX; //the right x coordinate for gradientVecs
		float squareX = (x / frequency) - leftSide + (x0 - leftLineX); //the current x coordinate within the current grid square

		for (int y = 0; y < height; y++) {
			//std::cout << "starting y loop " << y << std::endl;
			float worldY = y0 + y / frequency;
			//std::cout << "thisY is " << thisY << std::endl;
			int topSide = ceil(worldY) - bottomLineY;
			//std::cout << "topSide is " << topSide << std::endl;
			//std::cout << "leftSide is " << leftSide << std::endl;
			int bottomSide = floor(worldY) - bottomLineY;
			float squareY = (y / frequency) - bottomSide + (y0 - bottomLineY); //the current y coordinate within the current grid square
			//std::cout << "bottomSide is " << bottomSide << std::endl;

			//glm::vec2 toTopLeft = glm::vec2(floor(thisX), ceil(thisY));
			//glm::vec2 toTopLeft = glm::vec2(leftSide - midX, topSide - midY);
			glm::vec2 toTopLeft = glm::vec2(-squareX, 1 - squareY);
			//std::cout << "toTopLeft is " << toTopLeft.x << " " << toTopLeft.y << std::endl;
			//std::cout << leftSide - midX << std::endl;
			glm::vec2 toTopRight = glm::vec2(1 - squareX, 1 - squareY);
			glm::vec2 toBottomLeft = glm::vec2(-squareX, -squareY);
			glm::vec2 toBottomRight = glm::vec2(1 - squareX, -squareY);

			float topLeftDot = glm::dot(toTopLeft, gradientVecs[leftSide][topSide]);
			//std::cout << "topLeftDot is " << topLeftDot << std::endl;
			float topRightDot = glm::dot(toTopRight, gradientVecs[rightSide][topSide]);
			float bottomLeftDot = glm::dot(toBottomLeft, gradientVecs[leftSide][bottomSide]);
			float bottomRightDot = glm::dot(toBottomRight, gradientVecs[rightSide][bottomSide]);

			float lerpX1 = MathHelper::lerp(glm::vec2(0, topLeftDot), glm::vec2(1, topRightDot), squareX);
			float lerpX2 = MathHelper::lerp(glm::vec2(0, bottomLeftDot), glm::vec2(1, bottomRightDot), squareX);
			//std::cout << leftSide << " " << rightSide << " " << topSide << " " << bottomSide << std::endl;
			//std::cout << thisX << " " << thisY << " " << x << " " << y << std::endl;
			//std::cout << MathHelper::lerp(glm::vec2(topSide, lerpX1), glm::vec2(bottomSide, lerpX2), thisY) << std::endl;
			ret[x][y] = MathHelper::lerp(glm::vec2(1, lerpX1), glm::vec2(0, lerpX2), squareY);
		}

	}

	//std::cout << "noisemap gen time " << (glfwGetTime() - startTime) * 1000 << " milliseconds" << std::endl;
	
	return ret;
}


std::vector<std::vector<float>> PerlinNoise::makeHeightmapWithOctaves(int width, int height, glm::vec2 startPos, int seed, int octaves, float startFrequency, float lacunarity, float startAmplitude, float persistence) {

	std::vector<std::vector<float>> ret;
	std::vector<std::vector<float>> current;

	ret = makeHeightmap(width, height, startPos.x, startPos.y, seed, startFrequency, startAmplitude);

	float frequency = startFrequency;
	float amplitude = startAmplitude;

	srand(seed);
	std::vector<glm::vec2> octaveOffsets;
	octaveOffsets.push_back(glm::vec2(0, 0));
	for (int i = 1; i < octaves; i++) {
		octaveOffsets.push_back(glm::vec2(rand() % 200000 - 100000, rand() % 200000 - 100000));
	}

	for (int i = 1; i < octaves; i++) {
		frequency *= lacunarity;
		amplitude *= persistence;
		current = makeHeightmap(width, height, startPos.x + octaveOffsets[i].x, startPos.y + octaveOffsets[i].y, seed, frequency, amplitude);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				ret[x][y] += current[x][y];
			}
		}
	}

	return ret;

}

float PerlinNoise::getValue(float x, float y, int seed) {
	int rightX = ceil(x);
	int leftX = floor(x);
	int upY = ceil(y);
	int downY = floor(y);

	srand(((seed * 71) + (leftX * 73) + (upY * 79)) % 83);
	glm::vec2 topLeftVec = glm::vec2(rand() % 32, rand() % 32);
	srand(((seed * 71) + (rightX * 73) + (upY * 79)) % 83);
	glm::vec2 topRightVec = glm::vec2(rand() % 32, rand() % 32);
	srand(((seed * 71) + (leftX * 73) + (downY * 79)) % 83);
	glm::vec2 bottomLeftVec = glm::vec2(rand() % 32, rand() % 32);
	srand(((seed * 71) + (rightX * 73) + (downY * 79)) % 83);
	glm::vec2 bottomRightVec = glm::vec2(rand() % 32, rand() % 32);

	glm::vec2 toTopLeft = glm::vec2(leftX - x, upY - y);
	glm::vec2 toTopRight = glm::vec2(rightX - x, upY - y);
	glm::vec2 toBottomLeft = glm::vec2(leftX - x, downY - y);
	glm::vec2 toBottomRight = glm::vec2(rightX - x, downY - y);

	float topLeftDot = glm::dot(toTopLeft, topLeftVec);
	float topRightDot = glm::dot(toTopRight, topRightVec);
	float bottomLeftDot = glm::dot(toBottomLeft, bottomLeftVec);
	float bottomRightDot = glm::dot(toBottomRight, bottomRightVec);

	float lerpX1 = MathHelper::lerp(glm::vec2(leftX, topLeftDot), glm::vec2(rightX, topRightDot), x);
	float lerpX2 = MathHelper::lerp(glm::vec2(leftX, bottomLeftDot), glm::vec2(rightX, bottomRightDot), x);
	float lerpZ = MathHelper::lerp(glm::vec2(upY, lerpX1), glm::vec2(downY, lerpX2), y);

	return lerpZ;
}
