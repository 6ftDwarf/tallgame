#include "World.h"



World::World()
{
	seed = 1; //TEMPORARY
	renderDistance = 2; //Just in case I forget to set it elsewhere
	WORLD_PATH = ".\\TallgameWorld"; //Will be set elsewhere eventually
	save();
}


World::~World()
{
	save();
	for (int i = chunks.size() - 1; i >= 0; i--) {
		delete chunks.at(i);
	}
	for (int i = entities.size() - 1; i >= 0; i--) {
		delete entities.at(i);
	}
}


void World::tick(float deltaTime) {
	
	for (int i = 0; i < entities.size(); i++) {
		glm::vec3 vel = entities.at(i)->getVel();
		glm::vec3 pos = entities.at(i)->getPos();
		glm::vec3 size = entities.at(i)->getAABB()->getSize();

		//Friction and gravity
		if (entities.at(i)->isOnGround()) {

			vel = glm::vec3(vel.x, vel.y - (6.0f*deltaTime), vel.z);
		}
		else {
			vel = glm::vec3(vel.x, vel.y - (6.0f*deltaTime), vel.z);
		}

		float newX = pos.x + vel.x*deltaTime;
		float newY = pos.y + vel.y*deltaTime;
		float newZ = pos.z + vel.z*deltaTime;


		if (vel.y < 0) {
			float startX = floor(pos.x);
			float endX = pos.x + size.x;
			float startY = floor(newY);
			float endY = startY + 1;
			float startZ = floor(pos.z);
			float endZ = pos.z + size.z;

			if (checkCollide(glm::vec3(startX, startY, startZ), glm::vec3(endX, endY, endZ))) {
				newY = ceil(newY);
				vel = glm::vec3(vel.x, 0, vel.z);
				entities.at(i)->setOnGround(true);
			}
			else {
				entities.at(i)->setOnGround(false);
			}
		}

		if (vel.x > 0) {
			float startX = floor(newX) + size.x;
			float endX = startX + 1;
			float startY = floor(newY);
			float endY = newY + size.y;
			float startZ = floor(pos.z);
			float endZ = pos.z + size.z;

			if (checkCollide(glm::vec3(startX, startY, startZ), glm::vec3(endX, endY, endZ))) {
				newX = floor(newX);
				vel = glm::vec3(0, vel.y, vel.z);
			}

		}
		else if (vel.x < 0) {
			float startX = floor(newX);
			float endX = startX + 1;
			float startY = floor(newY);
			float endY = newY + size.y;
			float startZ = floor(pos.z);
			float endZ = pos.z + size.z;

			if (checkCollide(glm::vec3(startX, startY, startZ), glm::vec3(endX, endY, endZ))) {
				newX = ceil(newX);
				vel = glm::vec3(0, vel.y, vel.z);
			}
		}

		if (vel.z > 0) {
			float startX = floor(newX);
			float endX = newX + size.x;
			float startY = floor(newY);
			float endY = newY + size.y;
			float startZ = floor(newZ) + size.z;
			float endZ = startZ + 1;

			if (checkCollide(glm::vec3(startX, startY, startZ), glm::vec3(endX, endY, endZ))) {
				newZ = floor(newZ);
				vel = glm::vec3(vel.x, vel.y, 0);
			}

		}
		else if (vel.z < 0) {
			float startX = floor(newX);
			float endX = newX + size.x;
			float startY = floor(newY);
			float endY = newY + size.y;
			float startZ = floor(newZ);
			float endZ = startZ + 1;

			if (checkCollide(glm::vec3(startX, startY, startZ), glm::vec3(endX, endY, endZ))) {
				newZ = ceil(newZ);
				vel = glm::vec3(vel.x, vel.y, 0);
			}
		}

		if (vel.y > 0) {
			float startX = floor(pos.x);
			float endX = pos.x + size.x;
			float startY = floor(newY) + size.y;
			float endY = startY+1;
			float startZ = floor(pos.z);
			float endZ = pos.z + size.z;

			if (checkCollide(glm::vec3(startX, startY, startZ), glm::vec3(endX, endY, endZ))) {
				newY = floor(newY);
				vel = glm::vec3(vel.x, 0, vel.z);
			}

			entities.at(i)->setOnGround(false);

		}



		/*
		glm::vec3 newPos(pos.x, newY, pos.z);
		int posBlock = getBlock(floor(newPos.x), floor(newPos.y), floor(newPos.z));
		if (posBlock != 0) {
			if (vel.y > 0) {
				newY = floor(newPos.y);
			}
			else {
				newY = ceil(newPos.y);
			}
			newPos = glm::vec3(pos.x, newY, pos.z);
			vel = glm::vec3(vel.x, 0, vel.z);
		}
		
		glm::vec3 newPos = glm::vec3(newX, newY, pos.z);
		int posBlock = getBlock(floor(newPos.x), floor(newPos.y), floor(newPos.z));
		if (posBlock != 0) {
			if (vel.x > 0) {
				newX = floor(newPos.x);
			}
			else {
				newX = ceil(newPos.x);
			}
			newPos = glm::vec3(newX, newY, pos.z);
			vel = glm::vec3(0, vel.y, vel.z);
		}

		newPos = glm::vec3(newX, newY, newZ);
		posBlock = getBlock(floor(newPos.x), floor(newPos.y), floor(newPos.z));
		if (posBlock != 0) {
			if (vel.z > 0) {
				newZ = floor(newPos.z);
			}
			else {
				newZ = ceil(newPos.z);
			}
			newPos = glm::vec3(newX, newY, newZ);
			vel = glm::vec3(vel.x, vel.y, 0);
		}
		*/
		entities.at(i)->setPos(glm::vec3(newX, newY, newZ));
		entities.at(i)->setVel(vel);
	}



	for (int i = 0; i < playerEntities.size(); i++) {
		Entity* e = playerEntities.at(i);
		glm::vec3 ppos = e->getPrevPos();
		glm::vec3 cpos = e->getPos();
		if (floor(ppos.x / 16) != floor(cpos.x / 16) || 
			floor(ppos.y / 16) != floor(cpos.y / 16) ||
			floor(ppos.z / 16) != floor(cpos.z / 16)) {

			genChunk(floor(cpos.x / 16), floor(cpos.y / 16), floor(cpos.z / 16));

			genLoadList(glm::vec3(floor(cpos.x / 16), floor(cpos.y / 16), floor(cpos.z / 16)));
			genUnloadList(glm::vec3(floor(cpos.x / 16), floor(cpos.y / 16), floor(cpos.z / 16)));

		}
		/*
		if (loadList.size() == 0) {
			genLoadList(glm::vec3(floor(cpos.x / 16), floor(cpos.y / 16), floor(cpos.z / 16)));
		}
		*/

	}
	
	if (loadList.size() != 0 && getChunk(loadList.at(0).x, loadList.at(0).y, loadList.at(0).z) != nullptr) {
		while (getChunk(loadList.at(0).x, loadList.at(0).y, loadList.at(0).z) != nullptr) {
			loadList.erase(loadList.begin());
			if (loadList.size() == 0) { break; }
		}
	}
	if (loadList.size() != 0) {
		glm::vec3 loadpos = loadList.at(0);
		genChunk(loadpos.x, loadpos.y, loadpos.z);
		loadList.erase(loadList.begin());
	}
	if (unloadList.size() != 0) {
		glm::vec3 unloadpos = unloadList.at(0);
		removeChunk(unloadpos.x, unloadpos.y, unloadpos.z);
		unloadList.erase(unloadList.begin());
	}

}
void World::genLoadList(glm::vec3 startChunk) {
	loadList.clear();
	for (int y = 0; y <= renderDistance; y++) {
		if (y != 0) {
			loadList.push_back(glm::vec3(startChunk.x, startChunk.y + y, startChunk.z));
			loadList.push_back(glm::vec3(startChunk.x, startChunk.y - y, startChunk.z));
		}
		for (int z = 0; z <= renderDistance; z++) {
			if (z != 0) {
				loadList.push_back(glm::vec3(startChunk.x, startChunk.y, startChunk.z + z));
				loadList.push_back(glm::vec3(startChunk.x, startChunk.y, startChunk.z - z));
			}
			if (y != 0 && z != 0) {
				loadList.push_back(glm::vec3(startChunk.x, startChunk.y + y, startChunk.z + z));
				loadList.push_back(glm::vec3(startChunk.x, startChunk.y + y, startChunk.z - z));
				loadList.push_back(glm::vec3(startChunk.x, startChunk.y - y, startChunk.z + z));
				loadList.push_back(glm::vec3(startChunk.x, startChunk.y - y, startChunk.z - z));
			}
			for (int x = 0; x <= renderDistance; x++) {
				if (x != 0) {
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y, startChunk.z));
					loadList.push_back(glm::vec3(startChunk.x - x, startChunk.y, startChunk.z));
				}
				if (x != 0 && z != 0) {
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y, startChunk.z + z));
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y, startChunk.z - z));
					loadList.push_back(glm::vec3(startChunk.x - x, startChunk.y, startChunk.z + z));
					loadList.push_back(glm::vec3(startChunk.x - x, startChunk.y, startChunk.z - z));
				}
				if (x != 0 && y != 0) {
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y + y, startChunk.z));
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y - y, startChunk.z));
					loadList.push_back(glm::vec3(startChunk.x - x, startChunk.y + y, startChunk.z));
					loadList.push_back(glm::vec3(startChunk.x - x, startChunk.y - y, startChunk.z));
				}
				if (x != 0 && z != 0 && y != 0) {
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y + y, startChunk.z + z));
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y + y, startChunk.z - z));
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y - y, startChunk.z + z));
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y - y, startChunk.z - z));
					loadList.push_back(glm::vec3(startChunk.x - x, startChunk.y + y, startChunk.z + z));
					loadList.push_back(glm::vec3(startChunk.x - x, startChunk.y + y, startChunk.z - z));
					loadList.push_back(glm::vec3(startChunk.x - x, startChunk.y - y, startChunk.z + z));
					loadList.push_back(glm::vec3(startChunk.x - x, startChunk.y - y, startChunk.z - z));
				}
			}
		}
	}

	/*
	for (int x = -renderDistance; x <= renderDistance; x++) {
		for (int z = -renderDistance; z <= renderDistance; z++) {
			for (int y = -renderDistance; y <= renderDistance; y++) {

				if (x != 0 || z != 0 || y != 0) {
					loadList.push_back(glm::vec3(startChunk.x + x, startChunk.y + y, startChunk.z + z));
				}

			}
		}
	}
	*/

}
void World::genUnloadList(glm::vec3 startChunk) {
	unloadList.clear();
	int len = chunks.size();
	glm::vec3 cpos;
	for (int i = 0; i < len; ++i) {
		cpos = chunks.at(i)->getPos();
		if (abs(startChunk.x - cpos.x) > renderDistance ||
			abs(startChunk.y - cpos.y) > renderDistance || 
			abs(startChunk.z - cpos.z) > renderDistance) {
			unloadList.push_back(cpos);
		}
	}
}
bool World::checkCollide(glm::vec3 start, glm::vec3 end) {
	for (int x = start.x; x < end.x; x++) {
		for (int y = start.y; y < end.y; y++) {
			for (int z = start.z; z < end.z; z++) {

				if (getBlock(x, y, z) != 0) {
					return true;
				}

			}
		}
	}
	return false;
}


void World::loadWorld() {
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(WORLD_PATH.c_str())) != NULL) {
		while ((ent = readdir(dir)) != NULL) {
			std::cout << ent->d_name << std::endl;
		}
		closedir(dir);
	}
	else {
		CreateDirectory(WORLD_PATH.c_str(), NULL);
	}

	for (int i = 0; i < chunks.size(); ++i) {
		saveChunk(chunks.at(i));
	}
	std::cout << "WORLD LOADED\n";
}
Chunk* World::loadChunk(glm::vec3 pos) {
	Chunk* ret = NULL;

	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(WORLD_PATH.c_str())) != NULL) {
		
		std::string fileName = std::to_string(pos.x) + "_" + std::to_string(pos.y) + "_" + std::to_string(pos.z) + ".txt";
		
		while ((ent = readdir(dir)) != NULL) {
			//std::cout << ent->d_name << "     " << fileName << std::endl;
			if (ent->d_name == fileName) {
				
				ret = new Chunk(pos.x, pos.y, pos.z);
				int blocks[16][16][16];
				std::fstream file(WORLD_PATH + "\\" + fileName);
				std::string line;
				int x = 0;
				int y = 0;
				int z = -1;
				while (getline(file, line)) {
					if (z >= 0) {
						blocks[x][y][z] = stoi(line);
					}
					++z;
					if (z >= 16) {
						z = 0;
						++y;
					}
					if (y >= 16) {
						y = 0;
						++x;
					}
				}
				ret->setBlocks(blocks);
				break;
			}
		}
		
		closedir(dir);
	}
	else {
		CreateDirectory(WORLD_PATH.c_str(), NULL);
	}

	return ret;
}
void World::save() {

	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(WORLD_PATH.c_str())) != NULL) {
		/* print all the files and directories within directory */
		/*
		while ((ent = readdir(dir)) != NULL) {
			std::cout << ent->d_name << std::endl;
		}
		*/
		closedir(dir);
	}
	else {
		CreateDirectory(WORLD_PATH.c_str(), NULL);
	}

	for (int i = 0; i < chunks.size(); ++i) {
		saveChunk(chunks.at(i));
	}
	std::cout << "WORLD STORED\n";

}
void World::saveChunk(Chunk* c) {
	std::fstream file;
	glm::vec3 pos = c->getPos();
	file.open(WORLD_PATH + "\\" + std::to_string(pos.x) + "_" + std::to_string(pos.y) + "_" + std::to_string(pos.z) + ".txt", std::fstream::out | std::fstream::trunc);
	//   "..\\..\\LandwalkerWorlds"
	file << SAVE_VERSION << std::endl;
	for (int x = 0; x < 16; ++x) {
		for (int y = 0; y < 16; ++y) {
			for (int z = 0; z < 16; ++z) {
				file << c->getBlock(x, y, z) << std::endl;
			}
		}
	}
	file.close();
}


World::raycastReturn World::findBlockOnRay(glm::vec3 startPos, glm::vec3 camFront, float maxDist) {
	glm::vec3 ret = glm::vec3(0.1, 0.1, 0.1);

	float roundPosX, roundPosY, roundPosZ;
	if (startPos.x >= 0) {
		roundPosX = startPos.x + 0.5;
	}
	else {
		roundPosX = startPos.x - 0.5;
	}
	if (startPos.y >= 0) {
		roundPosY = startPos.y + 0.5;
	}
	else {
		roundPosY = startPos.y - 0.5;
	}
	if (startPos.z >= 0) {
		roundPosZ = startPos.z + 0.5;
	}
	else {
		roundPosZ = startPos.z - 0.5;
	}

	startPos = glm::vec3(startPos.x + 0.5, startPos.y + 0.5, startPos.z + 0.5);
	

	int mapX = int(roundPosX);
	int mapY = int(roundPosY);
	int mapZ = int(roundPosZ);
	

	float sideDistX, sideDistY, sideDistZ;

	//float deltaDistX = sqrt(1 + (camFront.y * camFront.y) / (camFront.x * camFront.x));
	//float deltaDistY = sqrt(1 + (camFront.x * camFront.x) / (camFront.y * camFront.y));
	float deltaDistX = sqrt((1 * 1)
		+ ((camFront.y * (1 / camFront.x)) * (camFront.y * (1 / camFront.x)))
		+ ((camFront.z * (1 / camFront.x)) * (camFront.z * (1 / camFront.x))));
	float deltaDistY = sqrt(((camFront.x * (1 / camFront.y)) * (camFront.x * (1 / camFront.y)))
		+ (1 * 1)
		+ ((camFront.z * (1 / camFront.y)) * (camFront.z * (1 / camFront.y))));
	float deltaDistZ = sqrt(((camFront.x * (1 / camFront.z)) * (camFront.x * (1 / camFront.z)))
		+ ((camFront.y * (1 / camFront.z)) * (camFront.y * (1 / camFront.z)))
		+ (1 * 1));
	float rayLength;

	int stepX, stepY, stepZ;
	bool hit = false;
	int side; //Obsolete?


	if (camFront.x < 0) {
		stepX = -1;
		sideDistX = (startPos.x - mapX) * deltaDistX;
	}
	else {
		stepX = 1;
		sideDistX = (mapX + 1.0 - startPos.x) * deltaDistX;
	}
	if (camFront.y < 0) {
		stepY = -1;
		sideDistY = (startPos.y - mapY) * deltaDistY;
	}
	else {
		stepY = 1;
		sideDistY = (mapY + 1.0 - startPos.y) * deltaDistY;
	}
	if (camFront.z < 0) {
		stepZ = -1;
		sideDistZ = (startPos.z - mapZ) * deltaDistZ;
	}
	else {
		stepZ = 1;
		sideDistZ = (mapZ + 1.0 - startPos.z) * deltaDistZ;
	}


	bool hasGoneX = false;
	bool hasGoneY = false;
	bool hasGoneZ = false;
	float distBeforeX = 0;
	float distBeforeY = 0;
	float distBeforeZ = 0;

	float totalDist = 0.0f;
	while (!hit && totalDist < maxDist) {
		if (sideDistX < sideDistY && sideDistX < sideDistZ) {
			totalDist = sideDistX;
			//if (!hasGoneX) {
				//totalDist = totalDist + sideDistX;
				//hasGoneX = true;
			//}
			sideDistX += deltaDistX;
			//totalDist += deltaDistX;
			mapX += stepX;
			side = 0;
		}
		else if (sideDistY < sideDistX && sideDistY < sideDistZ) {
			totalDist = sideDistY;

			sideDistY += deltaDistY;
			mapY += stepY;
			side = 1;
		}
		else {
			totalDist = sideDistZ;

			sideDistZ += deltaDistZ;
			mapZ += stepZ;
			side = 2;
		}

		if (getBlock(mapX, mapY, mapZ) != 0) { 
			hit = true;
			ret = glm::vec3(mapX, mapY, mapZ);
		}
	}

	glm::vec3 hitPoint = startPos + (camFront * totalDist);

	//std::cout << "------------------" << std::endl;
	//std::cout << totalDist << std::endl;
	//std::cout << camFront.x << "  " << camFront.y << "  " << camFront.z << std::endl;
	//std::cout << camFront.x* totalDist << "  " << camFront.y* totalDist << "  " << camFront.z* totalDist << std::endl;
	//std::cout << startPos.x << "  " << startPos.y << "  " << startPos.z << std::endl;
	//std::cout << ret.x << "  " << ret.y << "  " << ret.z << std::endl;
	//std::cout << hitPoint.x << "  " << hitPoint.y << "  " << hitPoint.z << std::endl;

	//std::cout << startPos.x + sideDistX << "  " << startPos.y + sideDistY << "  " << startPos.z + sideDistZ << std::endl;
	//std::cout << deltaDistX << "  " << deltaDistY << "  " << deltaDistZ << std::endl;
	//std::cout << sideDistX << "  " << sideDistY << "  " << sideDistZ << std::endl;
	
	return raycastReturn(ret, hitPoint);
}

void World::breakBlock(glm::vec3 pos) {
	int chunkX = pos.x / 16;
	if (int(pos.x) % 16 < 0 && chunkX < 1) { --chunkX; }
	int chunkY = pos.y / 16;
	if (int(pos.y) % 16 < 0 && chunkY < 1) { --chunkY; }
	int chunkZ = pos.z / 16;
	if (int(pos.z) % 16 < 0 && chunkZ < 1) { --chunkZ; }

	Chunk* chunk = getChunk(chunkX, chunkY, chunkZ);

	if (chunk != nullptr) {
		chunk->breakBlock(glm::vec3(pos.x - chunkX * 16, pos.y - chunkY * 16, pos.z - chunkZ * 16));
	}
}

void World::addBlock(glm::vec3 pos, int type) {
	int chunkX = pos.x / 16;
	if (int(pos.x) % 16 < 0 && chunkX < 1) { --chunkX; }
	int chunkY = pos.y / 16;
	if (int(pos.y) % 16 < 0 && chunkY < 1) { --chunkY; }
	int chunkZ = pos.z / 16;
	if (int(pos.z) % 16 < 0 && chunkZ < 1) { --chunkZ; }

	Chunk* chunk = getChunk(chunkX, chunkY, chunkZ);

	if (chunk != nullptr) {
		chunk->setBlock(pos.x - chunkX * 16, pos.y - chunkY * 16, pos.z - chunkZ * 16, type);
	}
}


int World::getBlock(int x, int y, int z) {
	int chunkX = x / 16;
	//if (x % 16 < 0 && chunkX == 0) { chunkX = -1; }
	if (x % 16 < 0) { chunkX -= 1; }
	int chunkY = y / 16;
	if (y % 16 < 0) { chunkY -= 1; }
	int chunkZ = z / 16;
	if (z % 16 < 0) { chunkZ -= 1; }

	Chunk* chunk = getChunk(chunkX, chunkY, chunkZ);

	if (chunk != nullptr) {
		return chunk->getBlock(x - chunkX * 16, y - chunkY * 16, z - chunkZ * 16);
	}
	else {
		return 0;
	}
}


Chunk* World::getChunk(int x, int y, int z) {
	Chunk* ret = nullptr;

	for (unsigned int i = 0; i < chunks.size(); i++) {
		glm::vec3 chunkPos = chunks.at(i)->getPos();
		if (chunkPos.x == x && chunkPos.y == y && chunkPos.z == z) {
			ret = chunks.at(i);
			break;
		}
	}
	
	return ret;
}

Chunk* World::genChunk(int x, int y, int z) {
	Chunk* ret = getChunk(x,y,z);

	if (ret == nullptr) {
		ret = loadChunk(glm::vec3(x, y, z));
		if (ret == nullptr) {
			ret = new Chunk(x, y, z);
			chprovider.buildChunk(ret, seed);
		}
		chunks.push_back(ret);
	}

	return ret;
}

void World::removeChunk(int x, int y, int z) {

	for (unsigned int i = 0; i < chunks.size(); i++) {
		glm::vec3 chunkPos = chunks.at(i)->getPos();
		if (chunkPos.x == x && chunkPos.y == y && chunkPos.z == z) {
			saveChunk(chunks.at(i));
			delete chunks.at(i);
			chunks.erase(chunks.begin() + i);
			break;
		}
	}

}

std::vector<Chunk*>* World::getChunks() { return &chunks; }


void World::addEntity(Entity* newEntity) { entities.push_back(newEntity); }

std::vector<Entity*>* World::getEntities() { return &entities; }


void World::addPlayerEntity(Entity* newEntity) { playerEntities.push_back(newEntity); }

void World::setRenderDistance(int rd) { renderDistance = rd; }