#include "MathHelper.h"



MathHelper::MathHelper()
{
}


MathHelper::~MathHelper()
{
}

float MathHelper::lerp(glm::vec2 a, glm::vec2 b, float x) {
	//return a.y + ( (x - a.x) * ((b.y - a.y) / (b.x - a.x)) ); //wikipedia version
	if (x == a.x) {
		return a.y;
	}
	else if (x == b.x) {
		return b.y;
	}
	else {
		return a.y + ((b.y - a.y) * ((x - a.x) / (b.x - a.x)));
	}
}