#pragma once

#ifndef MATHHELPER_H
#define MATHHELPER_H

#include<glm/glm.hpp>

class MathHelper
{
public:
	MathHelper();
	~MathHelper();

	static float lerp(glm::vec2 a, glm::vec2 b, float x);
	
};

#endif