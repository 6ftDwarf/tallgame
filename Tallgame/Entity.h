#pragma once

#ifndef ENTITY_H
#define ENTITY_H

#define GLEW_STATIC
#include <GL/glew.h>

#include<glm/glm.hpp>

#include<vector>
#include"AABB.h"

class Entity
{
public:
	Entity();
	~Entity();

	glm::vec3 getPos();
	glm::vec3 getPrevPos();
	void setPos(glm::vec3 newPos);
	AABB* getAABB();
	void setAABBsize(glm::vec3 newSize);
	bool isOnGround();
	void setOnGround(bool ong);

	glm::vec3 getVel();
	void setVel(glm::vec3 newVel);
	float getMass();
	void setMass(float newMass);

	glm::vec3 getBFront();
	void setBFront(glm::vec3 newFront);

	bool shouldRender();
	void setRender(bool r);
	GLuint getVAO();
	int getVertexCount();
private:

	// Physics data
	glm::vec3 pos;
	glm::vec3 prevPos;
	AABB aabb;
	bool onGround;

	glm::vec3 velocity;
	float mass;

	glm::vec3 bodyFront;

	// Render data
	bool renderBody = true;
	GLuint VBO, VAO;
	int vertexCount = 36;
	const GLfloat entityModel[288] = {
		-0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,
		0.5f,  1.5f, -0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 1.0f,
		0.5f,  1.5f, -0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 1.0f,
		-0.5f,  1.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 0.0f,

		-0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,
		0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 1.0f,
		0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 1.0f,
		-0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 0.0f,

		-0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,
		-0.5f,  1.5f, -0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 0.0f,
		-0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,

		0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,
		0.5f,  1.5f, -0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 0.0f,
		0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f,

		-0.5f,  1.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f,
		0.5f,  1.5f, -0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 1.0f,
		0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,
		0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  1.0f, 0.0f,
		-0.5f,  1.5f,  0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 0.0f,
		-0.5f,  1.5f, -0.5f,  1.0f, 1.0f, 1.0f,  0.0f, 1.0f
	};
};

#endif