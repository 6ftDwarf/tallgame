#include "TargetReticle.h"



TargetReticle::TargetReticle()
{
	shader = ShaderMaker(vertexShaderPath, fragmentShaderPath);

	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	float widthHeightRatio = 800.0f / 600.0f;

	GLfloat vertices[] = { //If this array is put in the header, it reacts to changes really weirdly
		-0.02f, -0.02f * widthHeightRatio, 0.0f,		0.0f, 0.0f,
		0.02f, -0.02f * widthHeightRatio, 0.0f,		1.0f, 0.0f,
		0.02f, 0.02f * widthHeightRatio, 0.0f,		1.0f, 1.0f,

		-0.02f, -0.02f * widthHeightRatio, 0.0f,		0.0f, 0.0f,
		-0.02f, 0.02f * widthHeightRatio, 0.0f,		0.0f, 1.0f,
		0.02f, 0.02f * widthHeightRatio, 0.0f,		1.0f, 1.0f
	};

	size_t data_len = sizeof(vertices);
	glBufferData(GL_ARRAY_BUFFER, data_len, vertices, GL_STATIC_DRAW);

	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid *)0);
	glEnableVertexAttribArray(0);
	//Texture attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);



	int imWidth, imHeight;
	unsigned char* image;

	// TEXTURE 1
	glGenTextures(1, &reticleTexture);
	glBindTexture(GL_TEXTURE_2D, reticleTexture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	image = SOIL_load_image(".\\Resources\\TallgameCrosshair.png", &imWidth, &imHeight, 0, SOIL_LOAD_RGBA);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imWidth, imHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);


	glBindTexture(GL_TEXTURE_2D, 0);
}


TargetReticle::~TargetReticle()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
}

void TargetReticle::useShaderAndTexture() { 
	shader.Use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, reticleTexture);
	glUniform1i(shader.getUniform("myReticleTexture"), 0);
}

void TargetReticle::closeTexture(){
	glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint TargetReticle::getVAO() { return VAO; }