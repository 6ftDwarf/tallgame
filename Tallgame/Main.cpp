

#define GLEW_STATIC
#include <GL/glew.h>

#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <vector>
#include <tuple>

#include "ShaderMaker.h"
#include "Chunk.h"
#include "World.h"
#include "Entity.h"
#include "BlockList.h"
#include "TargetBox.h"
#include "TargetReticle.h"


void key_callback(GLFWwindow* window, int key, int sancode, int action, int mode);
void do_movement(Entity* player);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouseUpdate();
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

const GLchar *VERSION = "Tallgame Alpha 1.0";

const GLint WIDTH = 800, HEIGHT = 600;

//const GLchar *vertexShaderPath = ".\\Resources\\VShaderBasic.txt";
const GLchar *vertexShaderPath = ".\\Resources\\VShaderChunk.txt";
//const GLchar *fragmentShaderPath = ".\\Resources\\FShaderBasic.txt";
const GLchar *fragmentShaderPath = ".\\Resources\\FShaderChunk.txt";

//Camera 
glm::vec3 cameraPos = glm::vec3(2.0f, 2.0f, 0.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
GLfloat lastX = WIDTH / 2;
GLfloat lastY = HEIGHT / 2;
int newX = lastX;
int newY = lastY;
GLfloat yaw = -90.0f;
// Yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right (due to how Eular angles work) so we initially rotate a bit to the left.
GLfloat pitch = 0.0f;
bool keys[1024];

// Delta time
GLfloat deltaTime = 0.0f; // Time between current frame and last frame
GLfloat lastFrame = 0.0f; // Time of last frame

World testWorld;
int renderDistance = 5; // Number of chunks to load in each direction, not including player's current chunk
glm::vec3 selectedBlock = glm::vec3(0.1, 0.1, 0.1);
glm::vec3 hitPoint = glm::vec3(0, 0, 0);

int main() {
	// ---------- INITIALIZATION ---------- //
	std::cout << "Hello, world!" << std::endl;
	glfwInit();
	std::cout << "GLFW Initialized" << std::endl;
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // Try and set OpenGL version to 3.3,
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // but use an older version if not available
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Use only modern OpenGL, not deprecated stuff
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	std::cout << "Window hints set" << std::endl;
	GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, VERSION, nullptr, nullptr);
	std::cout << "Attempted to create window" << std::endl;
	int screenWidth, screenHeight;
	std::cout << "Made int variables" << std::endl;
	glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
	// Get actual width of screen relative to density of screen
	std::cout << "Set framebuffer size" << std::endl;
	if (window == nullptr) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate(); // Terminate anything that's been initialized

		return EXIT_FAILURE;
	}

	const GLFWvidmode* vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwSetWindowPos(window, vidmode->width / 2 - WIDTH / 2, vidmode->height / 2 - HEIGHT / 2);

	glfwMakeContextCurrent(window);
	std::cout << "Set current context" << std::endl;
	glfwSetKeyCallback(window, key_callback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	std::cout << "Set callbacks" << std::endl;
	glewExperimental = GL_TRUE; // Not really experimental, just modern

	if (GLEW_OK != glewInit()) {
		std::cout << "Failed to initialize GLEW" << std::endl;

		return EXIT_FAILURE;
	}

	glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE); //TRIPPY :D
	//glBlendFunc(GL_ONE, GL_ONE);

	ShaderMaker shader(vertexShaderPath, fragmentShaderPath); // MUST come after glewInit()
	std::cout << "Created shader" << std::endl;
	glViewport(0, 0, screenWidth, screenHeight); //Define viewport dimensions

	glEnable(GL_DEPTH_TEST);

	BlockList::initBlocks();
	std::cout << "Initialized everything" << std::endl;
	// ---------- INITIALIZATION END ---------- //


	// ---------- TEXTURESTUFF ---------- //

	int imWidth, imHeight;
	unsigned char* image;

	GLuint blockTextures;

	// TEXTURE 1
	glGenTextures(1, &blockTextures);
	glBindTexture(GL_TEXTURE_2D, blockTextures);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	image = SOIL_load_image(".\\Resources\\BlockTextures.png", &imWidth, &imHeight, 0, SOIL_LOAD_RGB);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imWidth, imHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);


	glBindTexture(GL_TEXTURE_2D, 0);
	// ---------- TEXTURESTUFF END ----------
	std::cout << "Textures set up" << std::endl;
	GLuint modelLoc = shader.getUniform("model");
	GLuint viewLoc = shader.getUniform("view");
	GLuint projectionLoc = shader.getUniform("projection");
	glm::vec3 cubePositions[] = {
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(2.0f, 0.0f, 0.0f)
	};

	Chunk* chunk0;
	
	


	glm::mat4 projection;
	//projection = glm::perspective(glm::radians(45.0f), (float)screenWidth / screenHeight, 0.1f, 100.0f);
	projection = glm::perspective(glm::radians(45.0f), (float)screenWidth / screenHeight, 0.1f, (renderDistance * 16.0f)+16.0f);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // SO COOL!!!

	// ENTITY TESTING STUFF, TEMPORARY
	//Entity* testEntity = new Entity();
	//testEntity->setMass(10.0f);
	//testEntity->setPos(glm::vec3(5, 25, -1.5));
	//testWorld.addEntity(testEntity);

	Entity* playerEntity = new Entity();
	playerEntity->setMass(10.0f);
	playerEntity->setPos(glm::vec3(0.0f, 20.0f, 0.0f));
	playerEntity->setRender(false);
	testWorld.addEntity(playerEntity);
	testWorld.addPlayerEntity(playerEntity);

	testWorld.setRenderDistance(renderDistance);



	TargetBox tb;
	tb.setRenderBox(false);
	tb.setPos(glm::vec3(0, 15, 0));

	TargetReticle reticle;

	std::cout << "About to begin looping" << std::endl;
	// GAME LOOP
	while (!glfwWindowShouldClose(window)) {
		//std::cout << "Looping" << std::endl;
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		
		glfwPollEvents();
		do_movement(playerEntity);
		
		glm::vec3 front;
		front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		front.y = sin(glm::radians(0.0f));
		front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		playerEntity->setBFront(glm::normalize(front));

		testWorld.tick(deltaTime);

		mouseUpdate();

		cameraPos = glm::vec3(playerEntity->getPos().x, playerEntity->getPos().y+1, playerEntity->getPos().z);
		std::tie(selectedBlock, hitPoint) = testWorld.findBlockOnRay(cameraPos, cameraFront, 6.0f);

		glClearColor(0.5f, 0.7f, 0.9f, 1.0f); // Unnecessary, but adds some color to an otherwise blank window
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader.Use();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, blockTextures);
		glUniform1i(shader.getUniform("myTexture1"), 0);

		glm::mat4 view;
		view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

		//glBindVertexArray(VAO);
		/*
		for (GLuint i = 0; i < 10; i++) {
			glm::mat4 model;
			model = glm::translate(model, cubePositions[i]);
			//GLfloat angle = glm::radians(20.0f * i + 1);
			//model = glm::rotate(model, angle, glm::vec3(1.0f, sin(glfwGetTime()), cos(glfwGetTime())));
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

			glDrawArrays(GL_TRIANGLES, 0, 36);
		}
		*/

		/*
		glBindVertexArray(VAO); //This is the testing render block
		glm::mat4 model;
		model = glm::translate(model, glm::vec3(0, 0, 0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		*/

		std::vector<Chunk*>* chunks = testWorld.getChunks();
		for (unsigned int i = 0; i < chunks->size(); i++) {

			GLuint chunkVAO = chunks->at(i)->getVAO();
			glBindVertexArray(chunkVAO);

			glm::vec3 posMod = chunks->at(i)->getPos();
			glm::mat4 model;
			model = glm::translate(model, glm::vec3(posMod.x * 16, posMod.y * 16, posMod.z * 16));
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

			glDrawArrays(GL_TRIANGLES, 0, chunks->at(i)->getVertexCount());

			glBindVertexArray(0);

		}

		std::vector<Entity*>* entities = testWorld.getEntities();
		for (unsigned int i = 0; i < entities->size(); i++) {

			if (entities->at(i)->shouldRender()) {
				GLuint entityVAO = entities->at(i)->getVAO();
				glBindVertexArray(entityVAO);

				glm::vec3 posMod = entities->at(i)->getPos();
				glm::mat4 model;
				model = glm::translate(model, glm::vec3(posMod.x, posMod.y, posMod.z));
				glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

				glDrawArrays(GL_TRIANGLES, 0, entities->at(i)->getVertexCount());

				glBindVertexArray(0);
			}

		}


		tb.setPos(selectedBlock);
		//tb.setPos(glm::vec3(0, 100, 0));
		if (tb.getPos() != glm::vec3(0.1, 0.1, 0.1)) {
			tb.useShader();
			tb.setModelLoc();
			tb.setViewLoc(view);
			tb.setProjectionLoc(projection);
			glBindVertexArray(tb.getVAO());

			glDrawArrays(GL_LINES, 0, 24);
			glBindVertexArray(0);
		}


		reticle.useShaderAndTexture();
		glBindVertexArray(reticle.getVAO());
		glDrawArrays(GL_TRIANGLES, 0, 6);
		reticle.closeTexture();


		glBindVertexArray(0);

		glfwSwapBuffers(window);
	}
	std::cout << "Program ending" << std::endl;
	BlockList::cleanList();

	glfwTerminate(); // cleanup

	return EXIT_SUCCESS;
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			keys[key] = true;
		else if (action == GLFW_RELEASE)
			keys[key] = false;
	}
}

void do_movement(Entity* player)
{

	// Camera controls
	//GLfloat cameraSpeed = 5.0f * deltaTime;
	GLfloat cameraSpeed = 2.0f;
	glm::vec3 vel = player->getVel();
	glm::vec3 pbf = player->getBFront();
	glm::vec3 norm = glm::normalize(glm::cross(pbf, cameraUp));

	//if (!player->isOnGround()) {
		//cameraSpeed = 0.5f;
	//}

	vel = glm::vec3(0, vel.y, 0);

	if (keys[GLFW_KEY_W])
		//cameraPos += cameraSpeed * cameraFront;
		//vel += cameraSpeed * cameraFront;
		//vel += cameraSpeed * player->getBFront();
		vel = glm::vec3(vel.x + (pbf.x * cameraSpeed), vel.y, vel.z + (pbf.z * cameraSpeed));
	if (keys[GLFW_KEY_S])
		//vel -= cameraSpeed * cameraFront;
		//vel -= cameraSpeed * player->getBFront();
		vel = glm::vec3(vel.x + (pbf.x * -cameraSpeed), vel.y, vel.z + (pbf.z * -cameraSpeed));
	if (keys[GLFW_KEY_A])
		//vel -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
		//vel -= glm::normalize(glm::cross(player->getBFront(), cameraUp)) * cameraSpeed;
		vel = glm::vec3(vel.x + (norm.x * -cameraSpeed), vel.y, vel.z + (norm.z * -cameraSpeed));
	if (keys[GLFW_KEY_D])
		//vel += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
		//vel += glm::normalize(glm::cross(player->getBFront(), cameraUp)) * cameraSpeed;
		vel = glm::vec3(vel.x + (norm.x * cameraSpeed), vel.y, vel.z + (norm.z * cameraSpeed));
	if (keys[GLFW_KEY_SPACE] && player->isOnGround())
		vel += glm::vec3(0, 4, 0);

	player->setVel(vel);

}

GLboolean firstMouse = true;
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse) {
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	newX = xpos;
	newY = ypos;
	mouseUpdate();
}
void mouseUpdate() {
	GLfloat xoffset = newX - lastX;
	GLfloat yoffset = lastY - newY; // Reversed because y coordinates go from bottom to top
	lastX = newX;
	lastY = newY;

	GLfloat sensitivity = 0.05f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	//yaw += xoffset;
	pitch += yoffset;

	yaw = std::fmod((yaw + xoffset), (GLfloat)360.0f);

	if (pitch > 89.0f) {
		pitch = 89.0f;
	}
	if (pitch < -89.0f) {
		pitch = -89.0f;
	}

	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	cameraFront = glm::normalize(front);
}


void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && selectedBlock != glm::vec3(0.1, 0.1, 0.1)) {
		testWorld.breakBlock(selectedBlock);
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS && selectedBlock != glm::vec3(0.1, 0.1, 0.1)) {
		glm::vec3 posMod = glm::vec3(0, 0, 0);

		//std::cout << "------------------------------------------" << std::endl;
		//std::cout << selectedBlock.x << "  " << selectedBlock.y << "  " << selectedBlock.z << std::endl;
		//std::cout << hitPoint.x << "  " << hitPoint.y << "  " << hitPoint.z << std::endl;

		if (hitPoint.x == selectedBlock.x) {
			posMod = glm::vec3(-1, 0, 0);
		}
		else if (hitPoint.x == selectedBlock.x + 1) {
			posMod = glm::vec3(1, 0, 0);
		}
		else if (hitPoint.y == selectedBlock.y) {
			posMod = glm::vec3(0, -1, 0);
		}
		else if (hitPoint.y == selectedBlock.y + 1) {
			posMod = glm::vec3(0, 1, 0);
		}
		else if (hitPoint.z == selectedBlock.z) {
			posMod = glm::vec3(0, 0, -1);
		}
		else if (hitPoint.z == selectedBlock.z + 1) {
			posMod = glm::vec3(0, 0, 1);
		}
		
		//std::cout << posMod.x << "  " << posMod.y << "  " << posMod.z << std::endl;
		std::cout << selectedBlock.x << "  " << selectedBlock.y << "  " << selectedBlock.z << std::endl;

		if (posMod != glm::vec3(0, 0, 0)) {
			testWorld.addBlock(selectedBlock + posMod, 2);
		}
	}

}