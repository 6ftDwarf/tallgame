#pragma once

#ifndef AABB_H
#define AABB_H

#include<glm/glm.hpp>


// Axis Aligned Bounding Box
class AABB
{
public:
	AABB();
	AABB(glm::vec3 startsize, glm::vec3 startPos);
	~AABB();

	bool collideCheck(AABB* aabb2);

	void setSize(glm::vec3 newsize);
	glm::vec3 getSize();
	void setPos(glm::vec3 newPos);
	glm::vec3 getPos();
private:
	glm::vec3 size;
	glm::vec3 pos;
};

#endif