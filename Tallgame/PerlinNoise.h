#pragma once

#ifndef PERLINNOISE_H
#define PERLINNOISE_H

#include<array>
#include<vector>
#include<iostream>
#include<time.h>

#include<glm/glm.hpp>
#include<GLFW/glfw3.h>

#include"MathHelper.h"


class PerlinNoise
{
public:
	PerlinNoise();
	~PerlinNoise();

	static std::vector<std::vector<float>> makeHeightmap(int width, int height, float x0, float y0, int seed, float frequency, int heightScale);
	static std::vector<std::vector<float>> makeHeightmapWithOctaves(int width, int height, glm::vec2 startPos, int seed, int octaves, float startFrequency, float lacunarity, float startAmplitude, float persistence);
	static float getValue(float x, float y, int seed);
private:

};

#endif